var gulp      = require('gulp');
var sass      = require('gulp-sass');
var path      = require('path');
var minify    = require('gulp-minify');
var cleanCss  = require('gulp-clean-css');
var concat    = require('gulp-concat');

var paths = {
  css: [
    './dev/css/*.scss',
    './dev/css/**/*.scss'
  ],
  vendorCss: [
    './dev/css/vendor/*.css'
  ],
  jquery: [
    './dev/js/jquery/*.js'
  ],
  angular: [
    './dev/js/jquery/*.js'
  ],
  controllers: [
    './dev/js/controllers/*.js',
    './dev/js/controllers/**/*.js'
  ],
  vendor: [
    './dev/js/vendor/*.js',
    './dev/js/vendor/**/*.js'
  ],
  main: [
    './dev/js/*.js'
  ]
};

gulp.task('jquery', function(done) {
  gulp.src(paths.jquery)
  .pipe(concat('jquery.js'))
  .pipe(minify({
    ext:{
      min:'.js'
    },
    noSource: true  
  }))
  .pipe(gulp.dest('dist/js'))
  .on('end', done);
});

gulp.task('angular', function(done) {
  gulp.src(paths.jquery)
  .pipe(concat('angular.js'))
  .pipe(minify({
    ext:{
      min:'.js'
    },
    noSource: true  
  }))
  .pipe(gulp.dest('dist/js'))
  .on('end', done);
});

gulp.task('controllers', function(done) {
  gulp.src(paths.jquery)
  .pipe(concat('controllers.js'))
  .pipe(minify({
    ext:{
      min:'.js'
    },
    noSource: true  
  }))
  .pipe(gulp.dest('dist/js'))
  .on('end', done);
});

gulp.task('vendorJS', function(done) {
  gulp.src(paths.vendor)
  .pipe(concat('vendor.js'))
  .pipe(minify({
    ext:{
      min:'.js'
    },
    noSource: true  
  }))
  .pipe(gulp.dest('dist/js'))
  .on('end', done);
});

gulp.task('js', function(done) {
  gulp.src(paths.main)
  .pipe(concat('main.js'))
  .pipe(minify({
    ext:{
      min:'.js'
    },
    noSource: true  
  }))
  .pipe(gulp.dest('dist/js'))
  .on('end', done);
});

gulp.task('sass', function () {
  return gulp.src(paths.css)
    .pipe(sass().on('error', sass.logError))
    .pipe(concat('style.css'))
    .pipe(cleanCss())
    .pipe(gulp.dest('dist/css'));
});

gulp.task('vendorCSS', function () {
  return gulp.src(paths.vendorCss)
    .pipe(concat('vendor.css'))
    .pipe(cleanCss())
    .pipe(gulp.dest('dist/css'));
});
 
gulp.task('default', ['jquery', 'angular','controllers', 'vendorJS', 'vendorCSS', 'js', 'sass']);

gulp.task('watch', ['jquery', 'angular', 'controllers','vendorJS', 'vendorCSS', 'js', 'sass'], function() {
	gulp.watch(paths.jquery, ['jquery']);
	gulp.watch(paths.angular, ['angular']);
  gulp.watch(paths.angular, ['controllers']);
  gulp.watch(paths.vendor, ['vendorJS']);
  gulp.watch(paths.vendorCss, ['vendorCSS']);
	gulp.watch(paths.main, ['js']);
	gulp.watch(paths.css, ['sass']);
});